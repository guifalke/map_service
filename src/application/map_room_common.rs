use log::debug;

use crate::map_server::MapRoom;

impl MapRoom {

    pub fn is_over_center_x(&self, center_x: i32) -> bool{
		return self.x_start >= center_x;
	}

	pub fn is_over_center_y(&self, center_y: i32) -> bool{
		return self.y_start >= center_y;
	}

	pub fn get_end_x(&self) -> i32 {
		return self.x_start + self.x_width;
	}

	pub fn get_end_y(&self) -> i32 {
		return self.y_start + self.y_height;
	}

	pub fn conflict_with_room(&self, room_to_compare: &MapRoom) -> bool {
	    if room_to_compare.z_index == 0 {
	    	return false;
	    }	
		let result: bool = self.x_start > room_to_compare.x_start
			&& self.x_start < room_to_compare.get_end_x()
			&& self.y_start > room_to_compare.y_start
			&& self.y_start < room_to_compare.get_end_y()
			&& room_to_compare.z_index != 0
			&& self.z_index > room_to_compare.z_index;
		debug!("conflict_with_room to test conflict result {}",result);
		return result;
	}
}