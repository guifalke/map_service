use crate::map_server::MapRoom;

use rand::rngs::ThreadRng;
use rand::Rng;
use log::debug;

#[derive(Debug)]
pub struct RandonWalkHandler {
    pub max_enum_type: i32,
    pub to_build_later: Vec<MapRoom>
}

struct BuildInfo {
    min_size: i32,
    max_size: i32,
}

pub enum BuildType {
    Mountain,
    Tree,
    River,
}

pub enum Side {
    North,
    West,
    South,
    East,
}

impl RandonWalkHandler {
    pub fn new () -> RandonWalkHandler {
        return RandonWalkHandler {
            max_enum_type: 2,
            to_build_later: Vec::new(),
        }
    }

    pub fn build_procedural_struct(
        &mut self,
        x_initial: i32,
        x_final: i32,
        y_initial: i32,
        y_final: i32,
    ) -> MapRoom {
        debug!(
            "Start build_procedural_struct x_inicial: {}, x_final: {}, y_initial: {}, y_final: {}",
            &x_initial,
            &x_final,
            &y_initial,
            &y_final
        );

        let mut rng: ThreadRng = rand::thread_rng();
        let room_center_x: i32 = rng.gen_range(x_initial..x_final);
        let room_center_y: i32 = rng.gen_range(y_initial..y_final);

        debug!("Random spot selected x: {}, y: {}.", room_center_x, room_center_y);
        
        //Flip a dice for structure
        let room_dice = rng.gen_range(1..self.max_enum_type);
        debug!("The dice was rolled for a room type {}.", &room_dice);
        let room_type: BuildType = self.select_build(room_dice);

        self.buid_chain_handler(&room_type, room_center_x, room_center_y);

        return self.build_based_build_type(&room_type, room_center_x, room_center_y);
    }

    fn get_randon_room_iteration(&self, t: &BuildType) -> i32{
        let mut rng: ThreadRng = rand::thread_rng();
        let info = self.select_build_info(t);
        return rng.gen_range(info.min_size..info.max_size);
    }

    fn build_based_build_type(
        &self, 
        room_type: &BuildType,
        room_center_x: i32,
        room_center_y: i32
    ) -> MapRoom {
        debug!("Room type selected {}:", self.build_to_string(&room_type));

        let mut rng: ThreadRng = rand::thread_rng();
        let random_interation: i32 = self.get_randon_room_iteration(&room_type);
        debug!(
            "This room will have {} interation.",&random_interation);

        let mut room: MapRoom = MapRoom{
            z_index: self.build_to_z_index(&room_type),
            x_start: room_center_x,
            y_start: room_center_y,
            x_width: 1,
            y_height: 1,
            kind: self.build_to_kind(&room_type).to_owned(), 
        };

        let mut head_direction: i32 = 0;

        for i in 1..random_interation {
            //Flip dice for direction
            let direction:Side;
            if i == 1 {
                head_direction = rng.gen_range(1..4);
                direction = self.select_direction(head_direction);
            } else if head_direction == 1 || head_direction == 2 {
                direction = self.select_direction(rng.gen_range(1..2));
            } else {
                direction = self.select_direction(rng.gen_range(3..4));
            }

            debug!(
                "Interaction {} is going to  direction: {}",
                i,
                self.direction_to_string(&direction)
            );

            match direction {
                Side::North => {
                    room.y_start -= 1; 
                },
                Side::West => {
                    room.x_width += 1;
                },
                Side::South => {
                    room.y_height += 1;
                },
                Side::East => {
                    room.x_start += 1;
                },
            }
        }
        return room;
    }

    fn buid_chain_handler(
        &mut self,
        e: &BuildType,
        room_center_x: i32,
        room_center_y: i32
    ) {
        let mut rng: ThreadRng = rand::thread_rng();
        match e {
            BuildType::Mountain => {
                let coin_flip = rng.gen_bool(0.5);
                if coin_flip {
                    debug!("A river it is being generated for this moutain.");
                    let r = self.build_based_build_type(
                        &BuildType::River,
                        room_center_x,
                        room_center_y
                    );
                    self.to_build_later.push(r);
                }
            },
            _ => {}          
        }
        
    }

    fn select_build(&self, value: i32) -> BuildType {
        return match value {
            1 => BuildType::Mountain,
            2 => BuildType::Tree,
            3 => BuildType::River,
            _ => BuildType::Mountain
        }
    }
    
    fn build_to_string(&self, e: &BuildType) -> &str {
        return match e {
            BuildType::Mountain => "Mountain",
            BuildType::Tree => "Tree",
            BuildType::River => "River",
        }
    }
    
    fn build_to_z_index(&self, e: &BuildType) -> i32 {
        return match e {
            BuildType::Mountain => 1,
            BuildType::Tree => 1,
            BuildType::River => 2,
            _ => 1,
        }
    }

    fn build_to_kind(&self, e: &BuildType) -> &str {
        return match e {
            BuildType::Mountain => "M",
            BuildType::Tree => "T",
            BuildType::River => "R",
        }
    }
    
    fn select_direction(&self, value: i32) -> Side {
        return match value {
            1 => Side::North,
            2 => Side::South,
            3 => Side::West,
            4 => Side::East,
            _ => Side::North,
        }
    }

    fn direction_to_string(&self, t:&Side) -> &str {
        return match t {
            Side::North => "North",
            Side::South => "South",
            Side::West => "West",
            Side::East => "East",
            _ => "North",
        }
    }

    fn select_build_info(&self, e: &BuildType) -> BuildInfo {
        return match e {
            BuildType::Mountain => BuildInfo{min_size: 2, max_size: 4,},
            BuildType::Tree => BuildInfo{min_size: 2, max_size: 5,},
            BuildType::River => BuildInfo{min_size: 5, max_size: 9,},
            _ => BuildInfo{min_size: 0, max_size: 0,},
        }
    } 
}
