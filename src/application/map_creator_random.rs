use crate::map_server::{MapInstance, MapRoom};

use log::debug;

use rltk::RandomNumberGenerator;
use std::collections::HashMap;

pub struct MapCreatorRandom{
	pub grid: MapInstance,
	room_workaround: Vec<MapRoom>,
	total_rooms: i32,
	current_rooms: i32,
}


impl MapCreatorRandom {
	pub fn new(x_w: i32, y_h: i32) -> MapCreatorRandom {
		debug!("New Map MapCreatorRandom");
		return MapCreatorRandom {
			grid: MapInstance{
				map_room: HashMap::new(),
				x_size: x_w,
				y_size: y_h,
			},
			room_workaround: Vec::new(),
			total_rooms: 1,
			current_rooms: 0,
		};
	}
	
	pub fn build(&mut self) {
		debug!("Build MapCreatorRandom");
		// Divide the whole map
		self.add_sub_areas(
			0,
			self.grid.x_size,
			0,
			self.grid.y_size
		);
		let mut rng = RandomNumberGenerator::new();

		while self.current_rooms < self.total_rooms {
			let random_room_index: usize = self.get_random_room(&mut rng);
			debug!("get_random_room result: {:?}", &self.room_workaround[random_room_index]);

			let candidate: MapRoom = self.get_random_sub_room(
				&self.room_workaround[random_room_index],
				&mut rng);

			debug!("get_random_sub_room result: {:?}", candidate);

			if self.is_possible(&candidate) {
				self.insert_room(candidate);				
    		}

        	self.current_rooms += 1;
		}
		debug!("this map build final room count: {:?}", self.grid.map_room.len());		
	}

	fn insert_room(&mut self, room: MapRoom){
		self.room_workaround.push(room.clone());
		if room.z_index > 0 {
			self.grid.map_room.insert(format!("{},{}", room.x_start, room.y_start),room);
		}
	}

	fn add_sub_areas(
		&mut self,
		x_start: i32,
		x_end: i32,
		y_start: i32,
		y_end: i32
	) {
		debug!(
			"add_sub_areas for  x_start: {}, x_end: {}, y_start: {}, y_end: {}",
			x_start,
			x_end,
			y_start,
			y_end
		);

    	let count_x = i32::abs(x_start - x_end);
    	let count_y = i32::abs(y_start - y_end);
    	let half_x = i32::max(count_x / 2, 1);
    	let half_y = i32::max(count_y / 2, 1);

    	debug!(
			"add_sub_areas for  count_x: {}, count_y: {}, half_x: {}, half_y: {}",
			count_x,
			count_y,
			half_x,
			half_y
		);

		let mut m:MapRoom = MapRoom{
    		z_index: 0,
    		x_start: x_start,
    		y_start: y_start,
    		x_width: half_x,
    		y_height: half_y,
    		kind: "DG".to_owned(), 
    	};

    	debug!("add_sub_areas for MapRoom: {:?}", m);
    	self.insert_room(m);

    	m = MapRoom{
    		z_index: 0,
    		x_start: x_start,
    		y_start: y_start + half_y,
    		x_width: half_x,
    		y_height: half_y,
    		kind: "GR".to_owned(),
    	};

		debug!("add_sub_areas for MapRoom: {:?}", m);
		self.insert_room(m);

    	m = MapRoom{
    		z_index: 0,
    		x_start: x_start + half_x,
    		y_start: y_start,
    		x_width: half_x,
    		y_height: half_y,
    		kind: "LI".to_owned(),
    	};

    	debug!("add_sub_areas for MapRoom: {:?}", m);
    	self.insert_room(m);

    	m = MapRoom{
    		z_index: 0,
    		x_start: x_start + half_x,
    		y_start: y_start + half_y,
    		x_width: half_x,
    		y_height: half_y,
    		kind: "RE".to_owned(),
    	};

    	debug!("add_sub_areas for MapRoom: {:?}", m);
    	self.insert_room(m);   	
	}

	fn get_random_room(&self, rng : &mut RandomNumberGenerator) -> usize {
		if self.grid.map_room.len() == 1 { return 0 as usize; }
		return (rng.roll_dice(1, self.room_workaround.len() as i32)-1) as usize;    	
	}

	fn get_random_sub_room(&self, room : &MapRoom, rng : &mut RandomNumberGenerator) -> MapRoom {
    	let mut result = room.clone();

    	let w = i32::max(3, rng.roll_dice(1, i32::min(room.x_width, 10))-1) + 1;
    	let h = i32::max(3, rng.roll_dice(1, i32::min(room.y_height, 10))-1) + 1;

    	result.x_start += rng.roll_dice(1, 6)-1;
    	result.y_start += rng.roll_dice(1, 6)-1;
    	result.x_width = w;
    	result.y_height = h;
    	result.z_index = room.z_index + 1;
    	result.kind = "BL".to_owned();

    	return result;
	}

	fn is_possible(&self, room : &MapRoom) -> bool {
    	debug!("Starting is_possible to add room : {:?}", room);

    	let mut can_build = true;

    	if self.grid.map_room.len() > 0 {
    		debug!("is_possible grid has {} rooms to test conflict.", self.grid.map_room.len());

    		for test_conflit_room in self.room_workaround.iter() {
    			debug!("is_possible comparing room {:?} with {:?} room to check.", test_conflit_room, room);
				//test if x and y if the room conflict     			
    			can_build = !room.conflict_with_room(test_conflit_room);
    			
    		}
		}
	    debug!("is_possible: {}", can_build);

	    return can_build;
	}
}