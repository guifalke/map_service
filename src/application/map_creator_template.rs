use crate::{application::randon_walk::RandonWalkHandler, map_server::{MapInstance, MapRoom}};
use crate::cache::map_cache_handler::{set_map_instance, get_map_instance};
use std::collections::HashMap;

use log::debug;

#[derive(Debug)]
pub struct MapCreatorTemplate{	
	pub grid: MapInstance,
	pub id: String,
	randon_walk_service: RandonWalkHandler,
	room_workaround: Vec<MapRoom>,
	structures_iso: Vec<MapRoom>,
	structures_unique: Vec<MapRoom>,
	total_room_build: i32,
	half_x: i32,
	half_y: i32,
}

impl MapCreatorTemplate {
	pub fn new(x_w: i32, y_h: i32) -> MapCreatorTemplate {
		debug!("New Map MapCreatorTemplate");
		return MapCreatorTemplate {
			id: "key".to_owned(),
			grid: MapInstance{
				map_room: HashMap::new(),
				x_size: x_w,
				y_size: y_h,
			},
			randon_walk_service: RandonWalkHandler::new(),
			room_workaround: Vec::new(),
			structures_iso: Vec::new(),
			structures_unique: Vec::new(),
			total_room_build: 20,
			half_x: 0,
			half_y: 0,
		};
	}

	pub fn empty(key: String) -> MapCreatorTemplate {
		return MapCreatorTemplate {
			id: key.to_owned(),
			grid: MapInstance{
				map_room: HashMap::new(),
				x_size: 0,
				y_size: 0,
			},
			randon_walk_service: RandonWalkHandler::new(),
			room_workaround: Vec::new(),
			structures_iso: Vec::new(),
			structures_unique: Vec::new(),
			total_room_build: 20,
			half_x: 0,
			half_y: 0,
		};
	}

	pub fn get_by_id(key: String) -> MapCreatorTemplate {
		get_map_instance(key);
		return MapCreatorTemplate::new(0,0);
	}
	
	pub fn build(&mut self) {
		debug!("Build MapCreator start.");

		self.add_sub_areas(
			0,
			self.grid.x_size,
			0,
			self.grid.y_size
		);		

		self.get_base();
		self.get_base_enemy();
		let first_room = self.room_workaround.remove(0);
		//do all the logic for iso maps

		for _ in 0..self.total_room_build {
			let random_room =self.randon_walk_service.build_procedural_struct(
				first_room.x_start,
				first_room.get_end_x(),
				first_room.y_start,
				first_room.get_end_y()
			);
			if self.is_possible(&random_room) {
				self.structures_iso.push(random_room);
			}
		}

		while let Some(rest) = self.randon_walk_service.to_build_later.pop() {
			if self.is_possible(&rest){
				self.structures_iso.push(rest);
			}
		}

		self.room_workaround.insert(0, first_room);

		//get random point in user sub area
		self.process_structures_iso();
		self.process_structures_unique();

		debug!("Insert into cache with key {}.", self.id);
		set_map_instance(self);
		debug!("Build MapCreator end.");
	}

	fn get_base(&mut self){
		self.structures_iso.push(
			MapRoom{
				z_index: 1,
				x_start: self.half_x - 1,
				y_start: 0,
				x_width: 3,
				y_height: 2,
				kind: "B".to_owned(),
			}
		);
	}

	fn get_base_enemy(&mut self){
		self.structures_unique.push(
			MapRoom{
				z_index: 1,
				x_start: self.half_x - 1,
				y_start: self.half_y - 1,
				x_width: 3,
				y_height: 2,
				kind: "BE".to_owned(), 
			}
		);
	}

	fn process_structures_iso(&mut self){
		debug!("Start process_structures_iso");
		let mut to_add_room: Vec<MapRoom> = Vec::new(); 

		for st in self.structures_iso.iter() {
			// for each structure iso
			for room in self.room_workaround.iter() {
				debug!("room base to add isometric rooms: x:{} y:{}",&room.x_start, &room.y_start);
				// add in each subset
				if room.z_index == 0 {
 					let mut new_room: MapRoom = MapRoom{
						z_index: st.z_index,
						x_start: room.x_start + st.x_start,
						y_start: room.y_start + st.y_start,
						x_width: st.x_width,
						y_height: st.y_height,
						kind: st.kind.to_owned(), 
					};

					if room.is_over_center_y(self.half_y){
						debug!(
							"next sub_structure is over the center y so y_start is ({} - {} -{}) = {}",
							room.get_end_y(),
							st.y_start,
							st.y_height,
							room.get_end_y() - st.y_start - st.y_height
						);
						new_room.y_start = room.get_end_y() - st.y_start - st.y_height;
					}

					debug!("add_sub_structure iso for MapRoom: {:?}", new_room);

					to_add_room.push(new_room);
				}
			}
		}

		while let Some(r) = to_add_room.pop() {
			self.insert_room(r);
		}
	}

	fn process_structures_unique(&mut self){
		while let Some(r) = self.structures_unique.pop() {
			self.insert_room(r);
		}
	}

	fn insert_room(&mut self, room: MapRoom){
		self.room_workaround.push(room.clone());
		if room.z_index > 0 {
			self.grid.map_room.insert(format!("{},{}", room.x_start, room.y_start),room);
		}
	}

	fn add_sub_areas(
		&mut self,
		x_start: i32,
		x_end: i32,
		y_start: i32,
		y_end: i32
	) {
		debug!(
			"add_sub_areas for  x_start: {}, x_end: {}, y_start: {}, y_end: {}",
			x_start,
			x_end,
			y_start,
			y_end
		);

    	let count_x = i32::abs(x_start - x_end);
    	let count_y = i32::abs(y_start - y_end);
    	let half_x = i32::max(count_x / 2, 1);
		self.half_x = half_x;
    	let half_y = i32::max(count_y / 2, 1);
		self.half_y = half_y;

    	debug!(
			"add_sub_areas for  count_x: {}, count_y: {}, width_x: {}, half_y: {}",
			count_x,
			count_y,
			count_x,
			half_y
		);

		let mut m:MapRoom = MapRoom{
    		z_index: 0,
    		x_start: x_start,
    		y_start: y_start,
    		x_width: count_x,
    		y_height: half_y,
    		kind: "P1".to_owned(), 
    	};

    	debug!("add_sub_areas for MapRoom: {:?}", m);
    	self.insert_room(m);

    	m = MapRoom{
    		z_index: 0,
    		x_start: x_start,
    		y_start: y_start + half_y,
    		x_width: count_x,
    		y_height: half_y,
    		kind: "P2".to_owned(),
    	};

		debug!("add_sub_areas for MapRoom: {:?}", m);
		self.insert_room(m);    	   	
	}

	fn is_possible(&self, room : &MapRoom) -> bool {
    	debug!("Starting is_possible to add room : {:?}", room);

    	let mut can_build = true;

    	if self.grid.map_room.len() > 0 {
    		debug!("is_possible grid has {} rooms to test conflict.", self.grid.map_room.len());

    		for test_conflit_room in self.room_workaround.iter() {
    			debug!("is_possible comparing room {:?} with {:?} room to check.", test_conflit_room, room);
				//test if x and y if the room conflict     			
    			can_build = !room.conflict_with_room(test_conflit_room);
    			
    		}
		}
	    debug!("is_possible: {}", can_build);

	    return can_build;
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	/*use log4rs::config::{Appender, Root};
	use log4rs::Config;
	use log4rs::append::console::ConsoleAppender;
	use log::debug;
	use log::LevelFilter;

	fn init() {
		let stdout = ConsoleAppender::builder().build();

		let config = Config::builder()
			.appender(Appender::builder().build("stdout", Box::new(stdout)))
			.build(Root::builder().appender("stdout").build(LevelFilter::Debug))
			.unwrap();
		
		let _ = log4rs::init_config(config).unwrap();
	}
 */
    /* #[test]
	fn create_smaller_map_poosible(){
		//init();
		let mut service: MapCreatorTemplate = MapCreatorTemplate::new(
            30,
            30,
        );
		
		service.build();

		assert_eq!(service.grid.map_room.len() as i32, (service.total_room_build * 2) + 3);
	} */
}