use redis::{Client,Connection, Pipeline, RedisError, RedisResult};
use serde::de::value;
use crate::{application::map_creator_template::MapCreatorTemplate, map_server::MapRoom};
use log::debug;
use std::env;
use regex::Regex;

fn get_url() -> String{
    return match env::var_os("REDIS_URL") {
        Some(v) => v.into_string().unwrap(),
        None => "[::1]".to_owned(),
    };
}

fn get_port() -> String{
    return match env::var_os("REDIS_PORT") {
        Some(v) => v.into_string().unwrap(),
        None => "6379".to_owned(),
    };
}

pub fn set_map_instance(value: &mut MapCreatorTemplate) -> RedisResult<()> {
    let client: Client = redis::Client::open(format!("redis://{}:{}/", get_url(), get_port()))?;
    let mut con: Connection = client.get_connection()?;
    let key: String = format!("{}{}", "map_", value.id);
    let mut pipeline_exec = Pipeline::new();
    transform_map_in_hset(value, key, &mut pipeline_exec);
    let p  = pipeline_exec.query(&mut con)?;
    return RedisResult::Ok(());
}

pub fn get_map_instance(id: String) -> RedisResult<MapCreatorTemplate> {
    let client: Client = redis::Client::open(format!("redis://{}:{}/", get_url(), get_port()))?;
    let mut con: redis::Connection = client.get_connection()?;
    let key: String = format!("{}{}", "map_", id);
    return get_map_in_hget(key, &mut con);    
}

pub fn delete_map_instance(id: String) -> RedisResult<()>{
    let client: Client = redis::Client::open(format!("redis://{}:{}/", get_url(), get_port()))?;
    let mut con: redis::Connection = client.get_connection()?;
    let key: String = format!("{}{}", "map_", id);
    return redis::pipe().cmd("DEL").arg(key).query(&mut con);
}

fn transform_map_in_hset(
    value: &mut MapCreatorTemplate,
    key: String,
    pipe: &mut Pipeline
) {
    pipe.cmd("HSET").arg(&key).arg("grid.x_size").arg(value.grid.x_size).
    cmd("HSET").arg(&key).arg("grid.y_size").arg(value.grid.y_size);
    for (geo_key, room) in &value.grid.map_room {
        debug!("Add redis each room {}.", geo_key);
        pipe.cmd("HSET").arg(&key).arg(format!("{}.{}",geo_key, "z_index")).arg(room.z_index)
        .cmd("HSET").arg(&key).arg(format!("{}.{}",&geo_key, "x_start")).arg(room.x_start)
        .cmd("HSET").arg(&key).arg(format!("{}.{}",&geo_key, "x_width")).arg(room.x_width)
        .cmd("HSET").arg(&key).arg(format!("{}.{}",&geo_key, "y_start")).arg(room.y_start)
        .cmd("HSET").arg(&key).arg(format!("{}.{}",&geo_key, "y_height")).arg(room.y_height)
        .cmd("HSET").arg(&key).arg(format!("{}.{}",&geo_key, "kind")).arg(&room.kind);
    }     
}

fn get_map_in_hget(key: String, con: &mut Connection) -> RedisResult<MapCreatorTemplate> {
    let result_query:Result<Vec<Vec<String>>, RedisError> = redis::pipe().
        cmd("HGETALL").arg(&key).query(con);
    match result_query {        
        Ok(mut result)=> {
            debug!("Got result from result_query");
            let mut map_get:MapCreatorTemplate = MapCreatorTemplate::empty(key);
            debug!("Redis father result {:?}", &result);
            let child_result = result.first_mut().unwrap();
            debug!("Redis child result {:?}", &child_result);
            while let Some(output) = child_result.pop() {
                read_fields_to_map(output, child_result, &mut map_get);
            }
            debug!("Redis parsed result: {:?}", &map_get);
            return RedisResult::Ok(map_get);
        },
        Err(e) => {
            println!("get_map_in_hset {}", e);
            return RedisResult::Err(e);
        },
    }
}

fn read_fields_to_map(
    output: String,
    result: &mut Vec<String>,
    map_get:&mut MapCreatorTemplate
) {
    debug!("Value to add: {}", output);
    if let Some(parameter) = result.pop() {
        debug!("Paremeter: {}", parameter);
        let re = Regex::new(r"[\d+],[\d+]").unwrap();
        if re.is_match(&parameter){
            parse_map_room (&parameter, &output, map_get);
        } else if parameter == "grid.x_size" {
            map_get.grid.x_size = output.parse().unwrap();
        } else if parameter == "grid.y_size" {
            map_get.grid.y_size = output.parse().unwrap();
        }        
    }
}

fn parse_map_room (
    parameter: &String,
    value: &String,
    map_get:&mut MapCreatorTemplate)
{    
    let (left,rigth) = parameter.split_once('.').unwrap();
    let entry = map_get.grid.map_room.entry(left.to_owned())
        .or_insert(MapRoom {
            z_index: 0,
            x_start: 0,
            x_width: 0,
            y_start: 0,
            y_height: 0,
            kind: "".to_owned()
        });
    match rigth{
        "x_start" => entry.x_start = value.parse().unwrap(),
        "z_index" => entry.z_index = value.parse().unwrap(),
        "x_width" => entry.x_width = value.parse().unwrap(),
        "y_start" => entry.y_start = value.parse().unwrap(),
        "y_height" => entry.y_height = value.parse().unwrap(),
        "kind" => entry.kind = value.parse().unwrap(),
        _ => debug!("Property not found {}", parameter),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
	use log4rs::config::{Appender, Root};
	use log4rs::Config;
	use log4rs::append::console::ConsoleAppender;
	use log::debug;
	use log::LevelFilter;

	fn init() {
		let stdout = ConsoleAppender::builder().build();

		let config = Config::builder()
			.appender(Appender::builder().build("stdout", Box::new(stdout)))
			.build(Root::builder().appender("stdout").build(LevelFilter::Debug))
			.unwrap();
		
		let _ = log4rs::init_config(config).unwrap();
	}

    #[test]
    fn test_redis_get(){
        init();
        let result = get_map_instance("key".to_owned());
        match  result {
            Ok(r) => {
                debug!("Redis got result");
                assert!(true);
            },
            Err(e) => {
                debug!("Redis got error: {}", e);
                assert!(false);
            }
        }
    }

    //#[test]
    fn test_redis_insert(){
        let mut value = MapCreatorTemplate::new(30, 30);
        let mut room = MapRoom {
            kind: "T".to_owned(),
            x_start:1,
            x_width:1,
            y_height:1,
            y_start:1,
            z_index:1
        };
        value.grid.map_room.insert("1,1".to_owned(), room); 
        set_map_instance(&mut value);
    }

    //#[test]
    fn test_redis_delete(){
        let _ = delete_map_instance("key".to_owned());
    }
}