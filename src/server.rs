use tonic::{transport::Server, Request, Response, Status};

use log::debug;
use log::LevelFilter;

use log4rs::config::{Appender, Root};
use log4rs::Config;
use log4rs::append::console::ConsoleAppender;

mod map_server;
use map_server::map_server::{Map, MapServer};
use map_server::{MapVersion, MapInstance, CreateRequest, GetRequest};

mod application;
use crate::application::map_creator_random::MapCreatorRandom;
use crate::application::map_creator_template::MapCreatorTemplate;

mod cache;

#[derive(Debug, Default)]
pub struct MapImplementation {}

#[tonic::async_trait]
impl Map for MapImplementation {
    async fn version(
        &self,
        _request: Request<()>
    ) -> Result<Response<MapVersion>, Status> {
        debug!("Got a request version.");

        let reply: MapVersion = map_server::MapVersion {
            //message: format!("Hello {}!", request.into_inner().name).into(), // We must use .into_inner() as the fields of gRPC requests and responses are private
        	version: "127.0.0.1".to_owned(),
        };

        return Ok(Response::new(reply)); // Send back our formatted greeting
    }

    async fn create_procedural(
        &self,
        request: Request<CreateRequest>
    ) -> Result<Response<MapInstance>, Status> {
        debug!("Got a request: {:?}", request);

        let request_inner: CreateRequest = request.into_inner();

        let mut service: MapCreatorRandom = MapCreatorRandom::new(
            request_inner.x_size,
            request_inner.y_size,
        );

        service.build();

        return Ok(Response::new(service.grid));
    }

    async fn create_template(
        &self,
        request: Request<CreateRequest>
    ) -> Result<Response<MapInstance>, Status> {
        debug!("Got a request: {:?}", request);

        let request_inner: CreateRequest = request.into_inner();

        let mut service: MapCreatorTemplate = MapCreatorTemplate::new(
            request_inner.x_size,
            request_inner.y_size,
        );

        service.build();

        return Ok(Response::new(service.grid));
    }

    async fn get(
        &self,
        request: Request<GetRequest>
    ) -> Result<Response<MapInstance>, Status>{
        let request_inner: GetRequest = request.into_inner();
        let mut service: MapCreatorTemplate = MapCreatorTemplate::get_by_id(request_inner.id);
        return Ok(Response::new(service.grid));
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {

    let stdout = ConsoleAppender::builder().build();

    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .build(Root::builder().appender("stdout").build(LevelFilter::Debug))
        .unwrap();
    
    let _ = log4rs::init_config(config).unwrap();

    let addr = "[::0]:50051".parse()?;
    println!("Server started listening to {}", addr);
    let service = MapImplementation::default();

    Server::builder()
        .add_service(MapServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}