FROM rust:1.70.0-alpine as builder

RUN apk update && apk add --no-cache \
    make \
    protobuf-dev \
    cmake \
    #gcc \
    g++ \
    fontconfig-dev
#RUN apk add --no-cache binutils cmake libgcc musl-dev gcc g++
#RUN apk add --no-cache fontconfig-dev



WORKDIR /usr/src/mapservice
COPY . .
RUN rustup target add x86_64-unknown-linux-musl
RUN cargo build --target x86_64-unknown-linux-musl --release
RUN cargo install --path .

FROM alpine:latest
RUN apk update && rm -rf /var/lib/apt/lists/*

ENV REDIS_URL=redis-stack-server
ENV REDIS_PORT=6379

COPY --from=builder /usr/local/cargo/bin/map_server .
CMD ["./map_server"]